# small-flow-cell

A ~2.2 cm² flow cell for flow battery testing based of designs from the Brushett group at MIT and modifications by Kirk Smith.

Modifications include:
 - shortening of current collector tabs
 - redimensioning of O-ring slots (and corresponding slots in current collector) to be done by a standard endmill in a single pass (to eliminate need for CNC controll of mill)
 - ports are now 1/4-28 UNF fittings